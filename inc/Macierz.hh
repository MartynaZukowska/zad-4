#ifndef MACIERZ_HH
#define MACIERZ_HH

#include "rozmiar.h"
#include "Wektor.hh"
#include <iostream>
#include <algorithm>

/*
 *  
 *  
 */
template<template<typename Rodzaj, int ROZMIAR> class Wektor,typename Rodzaj, int ROZMIAR>
class Macierz {
  Wektor<Rodzaj,ROZMIAR>  _Tab[ROZMIAR];
public:
  Wektor<Rodzaj,ROZMIAR>  &operator[](int i);
  const Wektor<Rodzaj,ROZMIAR> &operator[](int i)const;
  void MacierzT();
  Wektor<Rodzaj,ROZMIAR> kolumna(int n)const;
  Rodzaj det()const;
  void Zamiana_kolumn(const Wektor<Rodzaj,ROZMIAR> &W,int n);
  Wektor<Rodzaj,ROZMIAR> operator *(const Wektor<Rodzaj,ROZMIAR> &W)const;
  Macierz operator*(const Macierz &Ma)const;

};


/* Przeciazenie << tak zeby odpowiednio pokazywal odpowiednie wektory tworzac 
 * z tego ladna macierz.
 */
template<template<typename Rodzaj,int ROZMIAR> class Wektor,typename Rodzaj,int ROZMIAR>
std::ostream &operator <<(std::ostream &wyswietl, const Macierz<Wektor,Rodzaj,ROZMIAR> &Ma)
{
    for(int i=0;i<ROZMIAR-1;++i)
      {
        wyswietl << Ma[i];
      }
    wyswietl <<Ma[ROZMIAR-1];
    return wyswietl;
}


/* Metoda przyjmuje wartosc n i za jej pomoca zwraca odpowiednia kolumne
 * macierzy (dzieki pomocniczemu wektorowi pom)
 */
template<template<typename Rodzaj, int ROZMIAR> class Wektor,typename Rodzaj, int ROZMIAR>
Wektor<Rodzaj,ROZMIAR> Macierz<Wektor,Rodzaj,ROZMIAR>::kolumna(int n)const
{
  Wektor<Rodzaj,ROZMIAR> pom;
  for(int i=0;i<ROZMIAR;i++)
    {
      pom[i]=_Tab[i][n];
    }
  return pom;
}

/*Transpozycja macierzy:
 * Metoda przyjmuje Macierz i zamienia w niej miejcem kolumny z wierszami.
 * Zwraca nowo powstala Macierz pom
 */
template<template<typename Rodzaj, int ROZMIAR> class Wektor,typename Rodzaj, int ROZMIAR>
void  Macierz<Wektor,Rodzaj,ROZMIAR>::MacierzT()
{
  Macierz<Wektor,Rodzaj,ROZMIAR> pom;
  pom=*this;
  for(int i=0;i<ROZMIAR;i++)
    {
      _Tab[i]=pom.kolumna(i);
    }
}


/* Przeciazenie >> tak zeby odpowiednio pobierala odpowiednie wektory tworzac 
 * z tego ladna macierz.
 */
template<template<typename Rodzaj, int ROZMIAR> class Wektor,typename Rodzaj, int ROZMIAR>
std::istream &operator >>(std::istream &wpisz, Macierz<Wektor,Rodzaj,ROZMIAR> &Ma)
{
  int pom;
  pom=0;
  while(pom!=ROZMIAR)
    {
      wpisz>>Ma[pom];
      pom++;
    }
  Ma.MacierzT();
      
  return wpisz;
}

/*Przeciezenie operatorow [] , aby byla mozliwosc odwolywania sie do prywatnej
 * tablicy klasy Macierz
 */
template<template<typename Rodzaj, int ROZMIAR> class Wektor,typename Rodzaj, int ROZMIAR>
Wektor<Rodzaj,ROZMIAR> &Macierz<Wektor,Rodzaj,ROZMIAR>::operator [] (int i)
{
   return _Tab[i];
}

/*Przeciezenie operatorow [] , aby byla mozliwosc odwolywania sie do prywatnej
 * tablicy klasy Macierz
 */
template<template<typename Rodzaj, int ROZMIAR> class Wektor,typename Rodzaj, int ROZMIAR>
const Wektor<Rodzaj,ROZMIAR> &Macierz<Wektor,Rodzaj,ROZMIAR>::operator [] (int i)const
{
   return _Tab[i];
}
/* Metoda zamienia odpowiednia kolumne w macierzy innym wektorem wykorzystujac
 * przy tym dwukrotnie metode transpozycji macierzy
 */
template<template<typename Rodzaj, int ROZMIAR> class Wektor,typename Rodzaj, int ROZMIAR>
void Macierz<Wektor,Rodzaj,ROZMIAR>::Zamiana_kolumn(const Wektor<Rodzaj,ROZMIAR> &W,int n)
{
  MacierzT();
  _Tab[n]=W;
  MacierzT();
}

/* Metoda ma za zadanie obliczyc wyznacznik macierzy
 *Wspomagamy sie pomocniczymi 
 *                          -x (zmienna do przechowywania tymczasowych wartosci)
 *                          -wiersze (liczy ilosc zmnian wierszy)
 *                          -pom (macierz bedaca kopia macierzy glownej)
 *
 * Sprawdzamy czy konkretne elementy macierzy nie sa zerem, a nastepnie jesli
 * nie jest zerujemy pierwszy niezerowy element danego wiersza, jesli jednak
 * jest poszukujemy w petli wiersza, ktorego odpowiednia skladowa nie jest zerem
 * i zamieniamy miejsca dwa odpowiednie wiersze oraz aktualizujemy licznik 
 * zmnian wierszy i przerwywamy wykonywanie petli. 
 *
 * Nastepnie zamieniamy jesli jest taka potrzeba znak wyznacznika
 *
 * Na koniec zwaracamy          - wyznacznik 
 */
template<template<typename Rodzaj, int ROZMIAR> class Wektor,typename Rodzaj, int ROZMIAR>
Rodzaj Macierz<Wektor,Rodzaj,ROZMIAR>::det()const
{
    Rodzaj x,wyznacznik=1.0; 
    int wiersze=0; 
    Macierz<Wektor,Rodzaj,ROZMIAR>  pom=*this;
    for(int i=0;i<ROZMIAR-1;++i)
    {
        for(int j=i+1;j<ROZMIAR;++j)
        {
            if(std::abs(pom._Tab[i][i])>0.000000001)
	      { 
                x=pom._Tab[j][i]/pom._Tab[i][i];
                pom._Tab[j]=pom._Tab[j]-(pom._Tab[i]*x); 
            }
            else // jesli konkretna skladowa jest zerem
            {
                for(int k=i+1;k<ROZMIAR;++k) 
                {
                    if(std::abs(pom._Tab[k][i])>0.0000000001)
		      {
                        std::swap(pom._Tab[i],pom._Tab[k]); 
                        ++wiersze; 
                        k=ROZMIAR;
                    }
                }
            }
        }
    }
    for(int i=0;i<ROZMIAR;++i)
      wyznacznik*=pom[i][i];
    if(wiersze%2!=0)
      {
	if(wyznacznik!=0)
	  {
	    wyznacznik=-wyznacznik;
	  }
      }
    return wyznacznik; 
}

template<template<typename Rodzaj, int ROZMIAR> class Wektor,typename Rodzaj, int ROZMIAR>
Wektor<Rodzaj,ROZMIAR> Macierz<Wektor,Rodzaj,ROZMIAR>::operator * (const Wektor<Rodzaj,ROZMIAR> &W)const
{
  Wektor<Rodzaj,ROZMIAR> pom;
    for(int i=0;i<ROZMIAR;++i)
      {
		 pom[i]=_Tab[i]*W;
      }
    return pom; 
}

template<template<typename Rodzaj, int ROZMIAR> class Wektor,typename Rodzaj, int ROZMIAR>
Macierz<Wektor,Rodzaj,ROZMIAR> Macierz<Wektor,Rodzaj,ROZMIAR>::operator*(const Macierz &Ma)const
{
  Macierz<Wektor,Rodzaj,ROZMIAR> Wynik;
  for(int i=0;i<ROZMIAR;i++)
    {
      for(int j=0;j<ROZMIAR;j++)
	{
	  
	  Wynik[i][j]=_Tab[i]*Ma.kolumna(j);
      	}
    }
  return Wynik;
  
}




#endif
