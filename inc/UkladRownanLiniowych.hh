#ifndef UKLADROWNANLINIOWYCH_HH
#define UKLADROWNANLINIOWYCH_HH
#include "rozmiar.h"
#include "Wektor.hh"
#include "Macierz.hh"
#include <iostream>


template <template <template <typename Rodzaj, int ROZMIAR> class Wektor,typename Rodzaj,int ROZMIAR> class Macierz,template <typename Rodzaj,int ROZMIAR> class Wektor, typename Rodzaj, int ROZMIAR>
class UkladR {
  Macierz<Wektor,Rodzaj,ROZMIAR> macierz;
  Wektor<Rodzaj,ROZMIAR> wektor;
  public:
  UkladR(): macierz(),wektor() {}
  UkladR(const Macierz<Wektor,Rodzaj,ROZMIAR> &Ma, const Wektor<Rodzaj,ROZMIAR> & W): macierz(Ma), wektor(W) {}
  const Macierz<Wektor,Rodzaj,ROZMIAR> & zwroc_macierz()const {return macierz;}
  const Wektor<Rodzaj,ROZMIAR> & zwroc_wektor()const {return wektor;}
  Wektor<Rodzaj,ROZMIAR> Cramer()const;
  
};
/*Przeciazenie operatora << dla klasy UkladR
 */
template <template <template <typename Rodzaj, int ROZMIAR> class Wektor,typename Rodzaj,int ROZMIAR> class Macierz,template <typename Rodzaj,int ROZMIAR> class Wektor, typename Rodzaj, int ROZMIAR>
std::ostream &operator <<(std::ostream &wyswietl, const UkladR<Macierz,Wektor,Rodzaj,ROZMIAR> &Uklad)
{
  using std::endl;
  wyswietl<<"\n Twoja macierz: \n";
  wyswietl<<Uklad.zwroc_macierz()<<"\n";
  wyswietl<<"Twoj wektor: \n";
  wyswietl<<Uklad.zwroc_wektor()<<"\n";
  wyswietl<<"Rozwiazanie: \n";
  //  wyswietl<<Uklad.Cramer();
  return wyswietl;
}

/*Przeciazenie operatora >> dla klasu UkladR
 *przypisanie nowego ukladu rownan do orginalu za pomoca konstruktora
 */
template <template <template <typename Rodzaj, int ROZMIAR> class Wektor,typename Rodzaj,int ROZMIAR> class Macierz,template <typename Rodzaj,int ROZMIAR> class Wektor, typename Rodzaj, int ROZMIAR>
std::istream &operator >>(std::istream &wpisz, UkladR<Macierz,Wektor,Rodzaj,ROZMIAR> &Uklad)
{
  Macierz<Wektor,Rodzaj,ROZMIAR> Ma;
  Wektor<Rodzaj,ROZMIAR> W;
      wpisz>>Ma;
      wpisz>>W;
      Uklad=UkladR<Macierz,Wektor,Rodzaj,ROZMIAR>(Ma,W);  //łaczenie ukladu w calosc
      return wpisz;
}

/*Metoda klasy Macierz rozwiazuje rownanie i podaje jego wyniki w postaci 
 * wektora. Robi to za pomoca 
 *                           - pomocniczej macierzy pom (kopi macierzy rownania)
 *                           -pomocniczego wektora W
 *                           - ukladu rownan 
 * Na poczatku sprawdzamy czy wyznacznik macierzy nie jest rowny zero, nastepnie
 * odliczamy poszczegolne skladniki wektora wynikowego i zamieniemy odpowiednie 
 * kolumny w macierzy z wektorem. Obliczamy dodatkowe wyznaczniki i powstale nam
 * wyniki wpisujemy w odpowiednie miejsca Wektora Wynik.Za kazdym razem po 
 * dzialaniach ustanawiamy macierzy pomocniczej jej poczatkowa wartosc.
 * Zwracamy Wektor Wynik. 
 */
template <template <template <typename Rodzaj, int ROZMIAR> class Wektor,typename Rodzaj,int ROZMIAR> class Macierz,template <typename Rodzaj,int ROZMIAR> class Wektor, typename Rodzaj, int ROZMIAR>
Wektor<Rodzaj,ROZMIAR> UkladR<Macierz,Wektor,Rodzaj,ROZMIAR>::Cramer()const
{
  Macierz<Wektor,Rodzaj,ROZMIAR> pom;
  Wektor<Rodzaj,ROZMIAR> W,Wynik;
  UkladR<Macierz,Wektor,Rodzaj,ROZMIAR> Uklad;
  Rodzaj wyznacznik,wyznacznik_pom;
  Uklad=*this;
  pom=Uklad.zwroc_macierz();
  W=Uklad.zwroc_wektor();
  wyznacznik=pom.det();
  if(std::abs(wyznacznik)>0.00000001)
    {
      for(int i=0;i<ROZMIAR;i++)
	{
	  pom.Zamiana_kolumn(W,i);
	  wyznacznik_pom=pom.det();
	  Wynik[i]=(wyznacznik_pom/wyznacznik);
	  pom=Uklad.zwroc_macierz();
	}
    }
  else
    {
      std::cout<<"Wyznacznik jest rowny 0 wiec musze tu zakonczyc dzialanie\n";
      exit(1);
    }
  return Wynik;
}




#endif
