#ifndef LZESPOLONA_HH
#define LZESPOLONA_HH

#include <iostream>

struct  LZespolona {
  double   re;    /*! Pole repezentuje czesc rzeczywista. */
  double   im;    /*! Pole repezentuje czesc urojona. */

  LZespolona &operator = (double  Liczba);
  public:
    LZespolona(): re(0.0), im(0.0) {} 
    LZespolona(const double & r, const double & i): re(r), im(i) {}
};


/*
 * Dalej powinny pojawic sie zapowiedzi definicji przeciazen operatorow
 */


LZespolona  operator + (LZespolona  Skl1,  LZespolona  Skl2);
LZespolona  operator - (LZespolona  Skl1,  LZespolona  Skl2);
LZespolona  operator * (LZespolona  Mnozna,  double  Mnoznik_Liczba);

std::ostream &operator << (std::ostream &StrmWyj, LZespolona  Zesp);
std::istream & operator >> (std::istream &Wpisz, LZespolona & l);
#endif
