#ifndef WEKTOR_HH
#define WEKTOR_HH
#include "rozmiar.h"
#include <iostream>

template <typename Rodzaj, int ROZMIAR>
class Wektor
{  Rodzaj Tab[ROZMIAR];
public:
  Rodzaj &operator[](int n);
  const Rodzaj &operator[](int n)const;
  Wektor<Rodzaj,ROZMIAR> operator /(const Rodzaj &Liczba)const;
  Wektor<Rodzaj,ROZMIAR> operator *(const Rodzaj &Liczba)const;
  Wektor<Rodzaj,ROZMIAR> operator +(const Wektor<Rodzaj,ROZMIAR> &W2)const;
  Wektor<Rodzaj,ROZMIAR> operator -(const Wektor<Rodzaj,ROZMIAR> &W2)const;
  Rodzaj operator *(const Wektor<Rodzaj,ROZMIAR> &W)const; //zapytaj o to
  Wektor<Rodzaj,ROZMIAR> operator %(const Wektor<Rodzaj,ROZMIAR> &W2)const;
 
 };



/*
 * Realizuje mnozenie Wektorowe dwoch wektorow.
 * Argumenty:
 *    Tab[i] - pierwszy element mnozenia,
 *    W - drugi element mnozenia.
 * Zwraca:
 *    Iloczyn dwoch elementow przekazanych jako parametry.
 */


template <typename Rodzaj, int ROZMIAR>
Rodzaj Wektor<Rodzaj,ROZMIAR>::operator * (const Wektor<Rodzaj,ROZMIAR> & W)const
{
    double sum=0.0;
    for(int i=0;i<ROZMIAR;++i)
        sum+=Tab[i]*W[i];
    return sum;
}


/*
 * Realizuje dodanie dwoch wektorow.
 * Argumenty:
 *    W - pierwszy skladnik dodawania,
 *    W2 - drugi skladnik dodawania.
 * Zwraca:
 *    Sume dwoch skladnikow przekazanych jako parametry.
 */
template <typename Rodzaj, int ROZMIAR>
Wektor<Rodzaj,ROZMIAR>  Wektor<Rodzaj,ROZMIAR>::operator + (const Wektor<Rodzaj,ROZMIAR> &W2)const
{
  Wektor Wynik;
  int pomocnicza;
  pomocnicza=0;
  while(pomocnicza!=ROZMIAR)
    {
      Wynik[pomocnicza]=Tab[pomocnicza]+W2[pomocnicza];
      pomocnicza++;
    }
  return Wynik;
}


/*
 * Realizuje mnozenie wektora i liczby.
 * Argumenty:
 *    W - pierwszy element mnozenia,
 *    Liczba - drugi element mnozenia.
 * Zwraca:
 *    Iloczyn dwoch elementow przekazanych jako parametry.
 */

template <typename Rodzaj, int ROZMIAR>
Wektor<Rodzaj,ROZMIAR> Wektor<Rodzaj,ROZMIAR>::operator * (const Rodzaj &Liczba)const
{
  Wektor Wynik;
  int pomocnicza;
  pomocnicza=0;
  while(pomocnicza!=ROZMIAR)
    {
      Wynik[pomocnicza]=Tab[pomocnicza]*Liczba;
      pomocnicza++;
    }
  return Wynik;
}

/*
 * Gdy Liczba nie jest rowna 0
 * Realizuje dzielenie wektora przez liczbe.
 * Argumenty:
 *    W - dzielna,
 *    Liczba - dzielnik.
 * Zwraca:
 *    Iloraz dwoch elementow przekazanych jako parametry.
 */
template <typename Rodzaj, int ROZMIAR>
Wektor<Rodzaj,ROZMIAR>  Wektor<Rodzaj,ROZMIAR>::operator / (const Rodzaj &Liczba)const
{
  Wektor Wynik;
  int pomocnicza;
  pomocnicza=0;
  if(Liczba==0)
    {
      std::cout<<"Nie mozez tak dzielic :( \n";
      exit(1);
    }
  while(pomocnicza!=ROZMIAR)
    {
      Wynik[pomocnicza]=Tab[pomocnicza]/Liczba;
      pomocnicza++;
    }
  return Wynik;
}




/*!
 * Realizuje odejmowanie dwoch wektorow.
 * Argumenty:
 *    W - odjemna,
 *    W2 - odjemnik.
 * Zwraca:
 *    Różnice dwoch elementow przekazanych jako parametry.
 */
template <typename Rodzaj, int ROZMIAR>
Wektor<Rodzaj,ROZMIAR>  Wektor<Rodzaj,ROZMIAR>::operator - (const Wektor<Rodzaj,ROZMIAR> &W2)const
{
  Wektor Wynik;
  int pomocnicza;
  pomocnicza=0;
  while(pomocnicza!=ROZMIAR)
    {
      Wynik[pomocnicza]=Tab[pomocnicza]-W2[pomocnicza];
      pomocnicza++;
    }
  return Wynik;
}

/* Przeciazenie << tak aby wypisywalo caly wektor*/
template <typename Rodzaj, int ROZMIAR>
std::ostream &operator <<(std::ostream &wyswietl, const Wektor<Rodzaj,ROZMIAR> &W)
{
  int pom;
  pom=0;
  wyswietl<<"|";
  while(pom!=ROZMIAR)
    {
      wyswietl<<" "<<W[pom];   
      pom++;
    }
  wyswietl<<" | \n";
  return wyswietl;
}

/*przeciazenie operatora >> dla klasy Wektor podanie nowych skladowych wektora
 * i zwrocenie obiektu klasy istream.
 */
template <typename Rodzaj, int ROZMIAR>
std::istream& operator >> (std::istream &wpisz, Wektor<Rodzaj,ROZMIAR> &W)
{
  int pom;
  pom=0;
  while(pom!=ROZMIAR)
    {
      wpisz>>W[pom];
      pom++;
    }
  return wpisz;
}



/*Przeciazenie [] tak aby mozna pozniej odwolywac sie do odpowiednich elementow
 * tablicy wekorow mimo ze jest ona w prywatnych */
template <typename Rodzaj, int ROZMIAR>
Rodzaj  &Wektor<Rodzaj,ROZMIAR>::operator [] (int n)
{
   return Tab[n];
}


/*Przeciazenie [] tak aby mozna pozniej odwolywac sie do odpowiednich elementow
 * tablicy wekorow mimo ze jest ona w prywatnych */
template <typename Rodzaj, int ROZMIAR>
const Rodzaj  &Wektor<Rodzaj,ROZMIAR>::operator [] (int n)const
{
   return Tab[n];
}



#endif
